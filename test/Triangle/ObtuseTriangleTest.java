package Triangle;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import Triangle.ObtuseTriangle;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joel.helling904
 */
public class ObtuseTriangleTest {
    
    public ObtuseTriangleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of isValid method, of class ObtuseTriangle.
     */
    @Test
    public void testIsValid() {
        System.out.println("isValid");
        ObtuseTriangle instance = new ObtuseTriangle(3,5,6);
        boolean expResult = true;
        boolean result = instance.isValid();
        assertEquals(expResult, result);
        instance = new ObtuseTriangle(2,3,3);
        expResult = false;
        result = instance.isValid();
        assertEquals(expResult, result);
    }
}