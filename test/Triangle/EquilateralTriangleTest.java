package Triangle;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import Triangle.EquilateralTriangle;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joel.helling904
 */
public class EquilateralTriangleTest {
    
    public EquilateralTriangleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of isValid method, of class EquilateralTriangle.
     */
    @Test
    public void testIsValid() {
        System.out.println("isValid");
        EquilateralTriangle instance = new EquilateralTriangle(2,2,2);
        boolean expResult = true;
        boolean result = instance.isValid();
        assertEquals(expResult, result);
        
        instance = new EquilateralTriangle(3,4,5);
        expResult = false;
        result = instance.isValid();
        assertEquals(expResult, result);
        
        instance = new EquilateralTriangle(5,5,5);
        expResult = true;
        result = instance.isValid();
        assertEquals(expResult, result);
    }
}