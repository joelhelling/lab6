package Triangle;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import Triangle.AcuteTriangle;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joel.helling904
 */
public class AcuteTriangleTest {
    
    public AcuteTriangleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of isValid method, of class AcuteTriangle.
     */
    @Test
    public void testIsValid() {
        System.out.println("isValid");
        AcuteTriangle instance = new AcuteTriangle(3,3,3);
        boolean expResult = true;
        boolean result = instance.isValid();
        assertEquals(expResult, result);
        instance = new AcuteTriangle(4,5,7);
        expResult = false;
        result = instance.isValid();
        assertEquals(expResult, result);
        instance = new AcuteTriangle(2,2,1);
        expResult = true;
        result = instance.isValid();
        assertEquals(expResult, result);
    }
}