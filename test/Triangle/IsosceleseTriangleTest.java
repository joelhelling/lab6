package Triangle;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import Triangle.IsosceleseTriangle;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joel.helling904
 */
public class IsosceleseTriangleTest {
    
    public IsosceleseTriangleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of isValid method, of class IsosceleseTriangle.
     */
    @Test
    public void testIsValid() {
        System.out.println("isValid");
        IsosceleseTriangle instance = new IsosceleseTriangle(99,99,1);
        boolean expResult = true;
        boolean result = instance.isValid();
        assertEquals(expResult, result);
        
        instance = new IsosceleseTriangle(4,3,4);
        expResult = true;
        result = instance.isValid();
        assertEquals(expResult, result);
    }
}