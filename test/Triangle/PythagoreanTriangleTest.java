package Triangle;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import Triangle.PythagoreanTriangle;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joel.helling904
 */
public class PythagoreanTriangleTest {
    
    public PythagoreanTriangleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of isValid method, of class PythagoreanTriangle.
     */
    @Test
    public void testIsValid() {
        System.out.println("isValid");
        PythagoreanTriangle instance = new PythagoreanTriangle(3,4,5);
        boolean expResult = true;
        boolean result = instance.isValid();
        assertEquals(expResult, result);
        instance = new PythagoreanTriangle(2,3,4);
        expResult = false;
        result = instance.isValid();
        assertEquals(expResult, result);
        instance = new PythagoreanTriangle(5,10,12);
        expResult = false;
        result = instance.isValid();
        assertEquals(expResult, result);
    }
}