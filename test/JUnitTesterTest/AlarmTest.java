package JUnitTesterTest;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import JUnitTester.Alarm;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joel.helling904
 */
public class AlarmTest {
    
    public AlarmTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getHour method, of class Alarm.
     */
    @Test
    public void testGetHour() {
        Alarm instance = new Alarm();
        assertTrue(instance.setTime(7, 30));
        int expResult = 7;
        int result = instance.getHour();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getMinute method, of class Alarm.
     */
    @Test
    public void testGetMinute() {

        Alarm instance = new Alarm();
        assertTrue(instance.setTime(7, 30));
        int expResult = 30;
        int result = instance.getMinute();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of setTime method, of class Alarm.
     */
    @Test
    public void testSetTime() {
        int hr = 7;
        int min = 30;
        Alarm instance = new Alarm();
        boolean expResult = true;
        boolean result = instance.setTime(hr, min);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of toString method, of class Alarm.
     */
    @Test
    public void testToString() {
        Alarm instance = new Alarm();
        assertTrue(instance.setTime(14,43));
        String expResult = "2:43PM";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
}