package JUnitTesterTest;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import JUnitTester.Account;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joel.helling904
 */
public class AccountTest {
    
    public AccountTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of deposit method, of class Account.
     */
    @Test
    public void testDeposit() {

        double amount = 1000.0;
        Account instance = new Account();
        instance.deposit(amount);
        assertEquals(1000,instance.getBalance(),0.1);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of withdraw method, of class Account.
     */
    @Test
    public void testWithdraw() {

        double amount = 1000.0;
        Account instance = new Account();
        instance.deposit(amount);
        double expResult = 1000.0;
        double result = instance.withdraw(amount);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getBalance method, of class Account.
     */
    @Test
    public void testGetBalance() {
        double amount = 1000.0;
        Account instance = new Account();
        instance.deposit(amount);
        double expResult = 1000.0;
        double result = instance.getBalance();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
    }
}