package JUnitTesterTest;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import JUnitTester.AddSubtract;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joel.helling904
 */
public class AddSubtractTest {
    
    public AddSubtractTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of add method, of class AddSubtract.
     */
    @Test
    public void testAdd() {
        AddSubtract as = new AddSubtract(3);
        
        as.add(5);
        assertEquals(8, as.getCurrentVal()); // note 8
        as.add(4);
        assertEquals(12, as.getCurrentVal());

    }

    /**
     * Test of subtract method, of class AddSubtract.
     */
    @Test
    public void testSubtract() {

        int a = 0;
        AddSubtract instance = new AddSubtract(0);
        instance.subtract(a);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of getCurrentVal method, of class AddSubtract.
     */
    @Test
    public void testGetCurrentVal() {

        AddSubtract instance = new AddSubtract(0);
        int expResult = 0;
        int result = instance.getCurrentVal();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }
}