package JUnitTesterTest;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import JUnitTester.DivideMultiply;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joel.helling904
 */
public class DivideMultiplyTest {
    
    public DivideMultiplyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of multiply method, of class DivideMultiply.
     */
    @Test
    public void testMultiply() {

        double a = 3.0;
        DivideMultiply instance = new DivideMultiply(3.0);
        instance.multiply(a);
        assertEquals(9.0, instance.getCurrentVal(), 0.0);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of divide method, of class DivideMultiply.
     */
    @Test
    public void testDivide() {

        double a = 5.0;
        DivideMultiply instance = new DivideMultiply(25.0);
        instance.divide(a);
        assertEquals(5.0,instance.getCurrentVal(),0.0);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of getCurrentVal method, of class DivideMultiply.
     */
    @Test
    public void testGetCurrentVal() {

        DivideMultiply instance = new DivideMultiply(2.0);
        double expResult = 2.0;
        double result = instance.getCurrentVal();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.

    }
}