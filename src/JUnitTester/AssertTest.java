package JUnitTester;

// Use assert to check if an absolute value is positive
import java.util.Scanner;

public class AssertTest
{
   public static void main( String args[] )
   {
      Scanner input = new Scanner( System.in );
      
      System.out.print( "Enter a number between 0 and 10: " );
      int number = input.nextInt();
      
      // assert that the absolute value is >= 0
      assert ( number >= 0 && number <= 10 ) : "bad number: " + number;
      
      System.out.printf( "You entered %d\n", number );
   } 
   
} 
/*
run:
Enter a number between 0 and 10: 11
Exception in thread "main" java.lang.AssertionError: bad number: 11
	at AssertTest.main(AssertTest.java:14)
Java Result: 1
BUILD SUCCESSFUL (total time: 6 seconds)
* */