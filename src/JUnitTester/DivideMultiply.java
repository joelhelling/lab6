package JUnitTester;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 
 */

 public class DivideMultiply {
     private double currentVal;
  
    /**
      * Constructor to initialize our member variable.
      */
     public DivideMultiply(double a) {
        currentVal = a;
     }

    /**
     * Multiply 'a' with our current value.
     */
    public void multiply(double a) {
       currentVal *= a;
    }

    /**
     * Divide 'a' from our current value.
     */
    public void divide(double a) {
       if (a == 0.0) {
          throw new java.lang.ArithmeticException("Can't divide by zero!");
       }

       currentVal /= a;
    }

    /**
     * Get the current value.
     */
    public double getCurrentVal() {
       return currentVal;
    }
 }