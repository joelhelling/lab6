/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle;
import java.util.*;
/**
 *
 * @author joel.helling904
 */
public class EquilateralTriangle extends Triangle {
    public EquilateralTriangle(double sa,double sb, double sc)
    {
        super(sa,sb,sc);
    }
    
    @Override
    public boolean isValid()
    {
        if(super.isValid())
            return this.sa == this.sb && this.sb == this.sc;
        else
            return false;
    }
}
