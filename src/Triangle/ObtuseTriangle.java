/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle;

import java.util.*;

/**
 *
 * @author joel.helling904
 */
public class ObtuseTriangle extends Triangle{
    public ObtuseTriangle(double sa, double sb, double sc)
    {
        super(sa,sb,sc);
    }
    
    @Override
    public boolean isValid()
    {
        if(super.isValid())
        {
            ArrayList list = super.getAnglesDegrees();
            Iterator iter = list.iterator();
            while(iter.hasNext())
            {
                if((double)iter.next() >= 90.0)
                    return true;
            }

            return false;
        }
        else
            return false;
    }
}
