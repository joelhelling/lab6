/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle;
import java.util.*;
/**
 *
 * @author joel.helling904
 */
public class AcuteTriangle extends Triangle {
    public AcuteTriangle(double sa, double sb, double sc)
    {
        super(sa,sb,sc);
    }
    
    @Override
    public boolean isValid()
    {
        if(super.isValid())
        {
            ArrayList list = super.getAnglesDegrees();
            Iterator iter = list.iterator();
            while(iter.hasNext())
            {
                if((double)iter.next() >= 90.0)
                    return false;
            }

            return true;
        }
        else
            return false;
    }
}
