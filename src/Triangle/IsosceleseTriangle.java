/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle;

import java.util.*;

/**
 *
 * @author joel.helling904
 */
public class IsosceleseTriangle extends Triangle{
  public IsosceleseTriangle(double sa, double sb, double sc)
    {
        super(sa,sb,sc);
    }
    
    @Override
    public boolean isValid()
    {
        if(super.isValid())
        {
            return this.sa == this.sb  
                   || this.sb == this.sc
                   || this.sa == this.sc;
        }
        else{
            return false;
        }
    }
}
