/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle;
import java.util.*;
/**
 *
 * @author joel.helling904
 */
public class Triangle {
    private static final double e = 0.001;
    protected double sa;
    protected double sb;
    protected double sc;
    private double aa;
    private double ab;
    private double ac;
    
    private boolean eq(double a, double b)
    {
        return (Math.abs(a-b) < this.e);
    }
    private boolean lt(double a, double b)
    {
        return (b-a > this.e);
    }
    public Triangle(double sa, double sb, double sc)
    {
        if(sa <= 0 )
            sa = 1;
        else 
            this.sa = sa;
        
        if(sb <= 0 )
            sb = 1;
        else 
            this.sb = sb;
        
        if(sc <= 0 )
            sc = 1;
        else 
            this.sc = sc;
        
        this.aa = Math.acos((Math.pow(sb,2) + Math.pow(sc,2) - Math.pow(sa,2) )/(2*sb*sc));
        this.ab = Math.acos((Math.pow(sa,2) + Math.pow(sc,2) - Math.pow(sb,2) )/(2*sa*sc));
        this.ac = Math.acos((Math.pow(sa,2) + Math.pow(sb,2) - Math.pow(sc,2) )/(2*sa*sb));
    }
    

    public double perimeter()
    {
        return sa+sb+sc;
    }
    public ArrayList getLengths()
    {
        ArrayList list = new ArrayList();
        list.add(this.sa);
        list.add(this.sb);
        list.add(this.sc);
        return list;
    }
    public ArrayList getAngles()
    {
        ArrayList list = new ArrayList();
        list.add(this.aa);
        list.add(this.ab);
        list.add(this.ac);
        return list;
    }
    
    public ArrayList getAnglesDegrees()
    {
        ArrayList list = new ArrayList();
        list.add(this.aa*180/Math.PI);
        list.add(this.ab*180/Math.PI);
        list.add(this.ac*180/Math.PI);
        return list;
    }
    
    public boolean isValid()
    {
        return (((this.sa + this.sb) > this.sc) 
                && ((this.sa + this.sc) >this.sb)
                && ((this.sb + this.sc) > this.sa));
    }
    
    public double area()
    {
        double p = (this.sa + this.sb + this.sc) /2;
        return Math.sqrt(p*(p-this.sa)*(p-this.sb)*(p-this.sc));
    }
}
